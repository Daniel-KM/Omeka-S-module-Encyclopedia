<?php declare(strict_types=1);

namespace Encyclopedia;

return [
    'form_elements' => [
        'invokables' => [
            Form\SiteSettingsFieldset::class => Form\SiteSettingsFieldset::class,
        ],
    ],
    'translator' => [
        'translation_file_patterns' => [
            [
                'type' => 'gettext',
                'base_dir' => dirname(__DIR__) . '/language',
                'pattern' => '%s.mo',
                'text_domain' => null,
            ],
        ],
    ],
    'api_assets' => [
        'allowed_media_types' => [
            'image/svg',
            'image/svgz',
            'image/svg+xml',
        ],
        'allowed_extensions' => [
            'svg',
        ],
    ],
    'encyclopedia' => [
        'site_settings' => [
            'encyclopedia_enabled' => false,

            'encyclopedia_site_name' => '',
            'encyclopedia_site_description' => '',
            'encyclopedia_site_logo' => '',

            'encyclopedia_publisher_url' => '',
            'encyclopedia_publisher_logo' => null,
            'encyclopedia_publisher_background' => null,

            'encyclopedia_home_background' => null,

            'encyclopedia_item_sets' => [],
            'encyclopedia_item_set_icon' => 'enc:icon',
            'encyclopedia_item_set_color' => 'enc:color',
            'encyclopedia_item_set_encyclopedia' => 'enc:isEncyclopedia',
            'encyclopedia_item_set_facet' => 'enc:isFacet',

            'encyclopedia_item_set_route' => 'enc:route',
            'encyclopedia_item_set_linked_label' => 'enc:linkedLabel',
            'encyclopedia_item_set_views' => 'enc:view',
            'encyclopedia_item_set_view_default' => 'enc:viewDefault',
            'encyclopedia_item_set_item_type' => 'enc:itemType',

            'encyclopedia_browse_custom_vocabs' => [],

            'encyclopedia_link_types' => [],

            // TODO Remove this key from the database.
            // 'encyclopedia_settings' => '',
        ],
    ],
];
