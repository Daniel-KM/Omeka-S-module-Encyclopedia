<?php declare(strict_types=1);

namespace Encyclopedia\Form;

use Common\Form\Element as CommonElement;
use Laminas\Form\Element;
use Laminas\Form\Fieldset;
use Omeka\Form\Element as OmekaElement;

class SiteSettingsFieldset extends Fieldset
{
    /**
     * @var string
     */
    protected $label = 'Encyclopedia'; // @translate

    protected $elementGroups = [
        'encyclopedia' => 'Encyclopedia', // @translate
    ];

    public function init(): void
    {
        $this
            ->setAttribute('id', 'encyclopedia')
            ->setOption('element_groups', $this->elementGroups)
            ->add([
                'name' => 'encyclopedia_enabled',
                'type' => Element\Checkbox::class,
                'options' => [
                    'element_group' => 'encyclopedia',
                    'label' => 'Enable front end for the theme', // @translate
                ],
                'attributes' => [
                    'id' => 'encyclopedia_enabled',
                ],
            ])

            ->add([
                'name' => 'encyclopedia_site_name',
                'type' => Element\Text::class,
                'options' => [
                    'element_group' => 'encyclopedia',
                    'label' => 'Name of the project (default: site title)', // @translate
                ],
                'attributes' => [
                    'id' => 'encyclopedia_site_name',
                ],
            ])
            ->add([
                'name' => 'encyclopedia_site_description',
                'type' => Element\Text::class,
                'options' => [
                    'element_group' => 'encyclopedia',
                    'label' => 'Short description for the footer (default: site summary)', // @translate
                ],
                'attributes' => [
                    'id' => 'encyclopedia_site_description',
                ],
            ])
            ->add([
                'name' => 'encyclopedia_site_logo',
                'type' => OmekaElement\Asset::class,
                'options' => [
                    'element_group' => 'encyclopedia',
                    'label' => 'Logo of the site (default: site thumbnail)', // @translate
                ],
                'attributes' => [
                    'id' => 'encyclopedia_site_logo',
                ],
            ])

            ->add([
                'name' => 'encyclopedia_publisher_url',
                'type' => CommonElement\OptionalUrl::class,
                'options' => [
                    'element_group' => 'encyclopedia',
                    'label' => 'Url of the publisher of the site', // @translate
                ],
                'attributes' => [
                    'id' => 'encyclopedia_publisher_url',
                ],
            ])
            ->add([
                'name' => 'encyclopedia_publisher_logo',
                'type' => OmekaElement\Asset::class,
                'options' => [
                    'element_group' => 'encyclopedia',
                    'label' => 'Logo of the publisher', // @translate
                ],
                'attributes' => [
                    'id' => 'encyclopedia_publisher_logo',
                ],
            ])
            ->add([
                'name' => 'encyclopedia_publisher_background',
                'type' => OmekaElement\Asset::class,
                'options' => [
                    'element_group' => 'encyclopedia',
                    'label' => 'Publisher background', // @translate
                ],
                'attributes' => [
                    'id' => 'encyclopedia_publisher_background',
                ],
            ])

            ->add([
                'name' => 'encyclopedia_home_background',
                'type' => OmekaElement\Asset::class,
                'options' => [
                    'element_group' => 'encyclopedia',
                    'label' => 'Home background', // @translate
                ],
                'attributes' => [
                    'id' => 'encyclopedia_home_background',
                ],
            ])

            ->add([
                'name' => 'encyclopedia_item_sets',
                'type' => CommonElement\OptionalItemSetSelect::class,
                'options' => [
                    'element_group' => 'encyclopedia',
                    'label' => 'Item sets of the project (default: site item sets)', // @translate
                    'info' => 'The initial project used collections for people, organisations, events, works, concepts, practices, and techniques. The list can contain collections for other pages too. They should have the resource template "Encyclopedia Collection" in order to get data about them.', // @translate
                ],
                'attributes' => [
                    'id' => 'encyclopedia_item_sets',
                    'class' => 'chosen-select',
                    'multiple' => true,
                    'data-placeholder' => 'Select item sets…', // @translate
                ],
            ])
            ->add([
                'name' => 'encyclopedia_item_set_icon',
                'type' => CommonElement\OptionalPropertySelect::class,
                'options' => [
                    'element_group' => 'encyclopedia',
                    'label' => 'Item set icon', // @translate
                    'info' => 'When a property is set, it should be a resource, a full url or a relative path to it.', // @translate
                    'term_as_value' => true,
                    'prepend_value_options' => [
                        'theme' => 'Icons from the theme ("icon_" + dcterms:identifier or id)', // @translate
                        'thumbnail' => 'Thumbnail of each collection', // @translate
                    ],
                ],
                'attributes' => [
                    'id' => 'encyclopedia_item_set_icon',
                    'required' => false,
                    'class' => 'chosen-select',
                    'data-placeholder' => 'Select source of the icon…', // @translate
                ],
            ])
            ->add([
                'name' => 'encyclopedia_item_set_color',
                'type' => CommonElement\OptionalPropertySelect::class,
                'options' => [
                    'element_group' => 'encyclopedia',
                    'label' => 'Item set color', // @translate
                    'term_as_value' => true,
                    'prepend_value_options' => [
                        'theme' => 'From the theme', // @translate
                    ],
                ],
                'attributes' => [
                    'id' => 'encyclopedia_item_set_color',
                    'required' => false,
                    'class' => 'chosen-select',
                    'data-placeholder' => 'Select source of the color…', // @translate
                ],
            ])
            ->add([
                'name' => 'encyclopedia_item_set_encyclopedia',
                'type' => CommonElement\OptionalPropertySelect::class,
                'options' => [
                    'element_group' => 'encyclopedia',
                    'label' => 'Item set is encyclopedia (has value 1 or true)', // @translate
                    'term_as_value' => true,
                    'prepend_value_options' => [
                        'theme' => 'From the theme', // @translate
                    ],
                ],
                'attributes' => [
                    'id' => 'encyclopedia_item_set_encyclopedia',
                    'required' => false,
                    'class' => 'chosen-select',
                    'data-placeholder' => 'Select property use to set if item set is encyclopedia…', // @translate
                ],
            ])
            ->add([
                'name' => 'encyclopedia_item_set_facet',
                'type' => CommonElement\OptionalPropertySelect::class,
                'options' => [
                    'element_group' => 'encyclopedia',
                    'label' => 'Item set is facet (has value 1 or true)', // @translate
                    'term_as_value' => true,
                    'prepend_value_options' => [
                        'theme' => 'From the theme', // @translate
                    ],
                ],
                'attributes' => [
                    'id' => 'encyclopedia_item_set_facet',
                    'required' => false,
                    'class' => 'chosen-select',
                    'data-placeholder' => 'Select property use to set if item set is a facet…', // @translate
                ],
            ])
            ->add([
                'name' => 'encyclopedia_item_set_route',
                'type' => CommonElement\OptionalPropertySelect::class,
                'options' => [
                    'element_group' => 'encyclopedia',
                    'label' => 'Item set slug', // @translate
                    'term_as_value' => true,
                    'prepend_value_options' => [
                        'theme' => 'From the theme', // @translate
                    ],
                ],
                'attributes' => [
                    'id' => 'encyclopedia_item_set_route',
                    'required' => false,
                    'class' => 'chosen-select',
                    'data-placeholder' => 'Select source of the slug…', // @translate
                ],
            ])
            ->add([
                'name' => 'encyclopedia_item_set_linked_label',
                'type' => CommonElement\OptionalPropertySelect::class,
                'options' => [
                    'element_group' => 'encyclopedia',
                    'label' => 'Item set linked elements label', // @translate
                    'term_as_value' => true,
                    'prepend_value_options' => [
                        'theme' => 'From the theme', // @translate
                    ],
                ],
                'attributes' => [
                    'id' => 'encyclopedia_item_set_linked_label',
                    'required' => false,
                    'class' => 'chosen-select',
                    'data-placeholder' => 'Select source of the linked elements label…', // @translate
                ],
            ])
            ->add([
                'name' => 'encyclopedia_item_set_views',
                'type' => CommonElement\OptionalPropertySelect::class,
                'options' => [
                    'element_group' => 'encyclopedia',
                    'label' => 'Item set view type', // @translate
                    'term_as_value' => true,
                    'prepend_value_options' => [
                        'theme' => 'From the theme', // @translate
                    ],
                ],
                'attributes' => [
                    'id' => 'encyclopedia_item_set_views',
                    'required' => false,
                    'class' => 'chosen-select',
                    'data-placeholder' => 'Select source of the view type…', // @translate
                ],
            ])
            ->add([
                'name' => 'encyclopedia_item_set_view_default',
                'type' => CommonElement\OptionalPropertySelect::class,
                'options' => [
                    'element_group' => 'encyclopedia',
                    'label' => 'Item set default view type', // @translate
                    'term_as_value' => true,
                    'prepend_value_options' => [
                        'theme' => 'From the theme', // @translate
                    ],
                ],
                'attributes' => [
                    'id' => 'encyclopedia_item_set_view_default',
                    'required' => false,
                    'class' => 'chosen-select',
                    'data-placeholder' => 'Select source of the default view type…', // @translate
                ],
            ])
            ->add([
                'name' => 'encyclopedia_item_set_item_type',
                'type' => CommonElement\OptionalPropertySelect::class,
                'options' => [
                    'element_group' => 'encyclopedia',
                    'label' => 'Item set item type', // @translate
                    'term_as_value' => true,
                    'prepend_value_options' => [
                        'theme' => 'From the theme', // @translate
                    ],
                ],
                'attributes' => [
                    'id' => 'encyclopedia_item_set_item_type',
                    'required' => false,
                    'class' => 'chosen-select',
                    'data-placeholder' => 'Select source of the item type…', // @translate
                ],
            ])

            ->add([
                'name' => 'encyclopedia_browse_custom_vocabs',
                'type' => CommonElement\CustomVocabsSelect::class,
                'options' => [
                    'element_group' => 'encyclopedia',
                    'label' => 'Custom vocabs (should be item sets)', // @translate
                    'custom_vocab_type' => 'resource',
                    'prepend_value_options' => [
                        'theme' => 'From the theme', // @translate
                    ],
                ],
                'attributes' => [
                    'id' => 'encyclopedia_browse_custom_vocabs',
                    'required' => false,
                    'multiple' => true,
                    'class' => 'chosen-select',
                    'data-placeholder' => 'Select indexes to browse…', // @translate
                ],
            ])

            ->add([
                'name' => 'encyclopedia_link_types',
                'type' => CommonElement\DataTextarea::class,
                'options' => [
                    'element_group' => 'encyclopedia',
                    'label' => 'Link types', // @translate
                    'key_value_separator' => ',',
                    'data_keys' => [
                        'term',
                        'title',
                        'dashStyle',
                        'strokeStyle',
                    ],
                ],
                'attributes' => [
                    'id' => 'encyclopedia_link_types',
                    'placeholder' => 'skos:broader, Lien générique
skos:narrower, Lien spécifique, 6 4
skos:closeMatch, Lien proche, 2 3
skos:broadMatch, Lien associé, 8 12, #FFFFFF
',
                ],
            ])
        ;
    }
}
