<?php declare(strict_types=1);

namespace Encyclopedia;

use Common\Stdlib\PsrMessage;

/**
 * @var Module $this
 * @var \Laminas\ServiceManager\ServiceLocatorInterface $services
 * @var string $newVersion
 * @var string $oldVersion
 *
 * @var \Omeka\Api\Manager $api
 * @var \Omeka\View\Helper\Url $url
 * @var \Omeka\Settings\Settings $settings
 * @var \Doctrine\DBAL\Connection $connection
 * @var \Doctrine\ORM\EntityManager $entityManager
 * @var \Omeka\Mvc\Controller\Plugin\Messenger $messenger
 */
$plugins = $services->get('ControllerPluginManager');
$url = $services->get('ViewHelperManager')->get('url');
$api = $plugins->get('api');
$settings = $services->get('Omeka\Settings');
$translate = $plugins->get('translate');
$connection = $services->get('Omeka\Connection');
$messenger = $plugins->get('messenger');
$entityManager = $services->get('Omeka\EntityManager');

if (!method_exists($this, 'checkModuleActiveVersion') || !$this->checkModuleActiveVersion('Common', '3.4.53')) {
    $message = new \Omeka\Stdlib\Message(
        $translate('The module %1$s should be upgraded to version %2$s or later.'), // @translate
        'Common', '3.4.53'
    );
    throw new \Omeka\Module\Exception\ModuleCannotInstallException((string) $message);
}

if (version_compare($oldVersion, '3.4.3', '<')) {
    $this->disableModule('FrontEnd');
}

if (version_compare($oldVersion, '3.4.4', '<')) {
    $config = $services->get('Config');
    $basePath = $config['file_store']['local']['base_path'] ?: (OMEKA_PATH . '/files');
    if (!$this->checkDestinationDir($basePath . '/encyclopedia')) {
        $message = new PsrMessage(
            'The directory "{directory}" is not writeable.', // @translate
            ['directory' => $basePath . '/encyclopedia']
        );
        throw new \Omeka\Module\Exception\ModuleCannotInstallException($translate((string) $message));
    }

    $settings->set('guest_terms_skip', true);

    $this->installAllResources();

    $message = new PsrMessage(
        'Update your config: the options are simplified in the site settings. It uses a new vocabulary "encyclopedia" and a template "Encyclopedia collection" for item sets.' // @translate
    );
    $messenger->addWarning($message);
}
