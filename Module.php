<?php declare(strict_types=1);

namespace Encyclopedia;

if (!class_exists(\Common\TraitModule::class)) {
    require_once dirname(__DIR__) . '/Common/TraitModule.php';
}

use Common\Stdlib\PsrMessage;
use Common\TraitModule;
use Laminas\EventManager\Event;
use Laminas\EventManager\SharedEventManagerInterface;
use Omeka\Api\Exception\NotFoundException;
use Omeka\Api\Representation\AssetRepresentation;
use Omeka\Module\AbstractModule;

/**
 * Encyclopedia
 *
 * @copyright Daniel Berthereau, 2021-2024
 * @license http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
 */
class Module extends AbstractModule
{
    use TraitModule;

    const NAMESPACE = __NAMESPACE__;

    protected function preInstall(): void
    {
        $services = $this->getServiceLocator();
        $plugins = $services->get('ControllerPluginManager');
        $translate = $plugins->get('translate');
        $translator = $services->get('MvcTranslator');

        if (!method_exists($this, 'checkModuleActiveVersion') || !$this->checkModuleActiveVersion('Common', '3.4.53')) {
            $message = new \Omeka\Stdlib\Message(
                $translate('The module %1$s should be upgraded to version %2$s or later.'), // @translate
                'Common', '3.4.53'
            );
            throw new \Omeka\Module\Exception\ModuleCannotInstallException((string) $message);
        }

        $config = $services->get('Config');
        $basePath = $config['file_store']['local']['base_path'] ?: (OMEKA_PATH . '/files');

        if (!$this->checkDestinationDir($basePath . '/encyclopedia')) {
            $message = new PsrMessage(
                'The directory "{directory}" is not writeable.', // @translate
                ['directory' => $basePath . '/encyclopedia']
            );
            throw new \Omeka\Module\Exception\ModuleCannotInstallException((string) $message->setTranslator($translator));
        }

        /** @var \Omeka\Settings\Settings $settings */
        $settings = $services->get('Omeka\Settings');
        $settings->set('guest_terms_skip', true);
    }

    protected function postUninstall(): void
    {
        $services = $this->getServiceLocator();
        $config = $services->get('Config');
        $basePath = $config['file_store']['local']['base_path'] ?: (OMEKA_PATH . '/files');
        $dirPath = $basePath . '/encyclopedia';
        $this->rmDir($dirPath);
    }

    public function attachListeners(SharedEventManagerInterface $sharedEventManager): void
    {
        $sharedEventManager->attach(
            \Omeka\Form\SiteSettingsForm::class,
            'form.add_elements',
            [$this, 'handleSiteSettings']
        );
    }

    public function handleSiteSettings(Event $event): void
    {
        $this->handleAnySettings($event, 'site_settings');

        /** @var \Laminas\Http\PhpEnvironment\Request $request */
        $services = $this->getServiceLocator();
        $request = $services->get('Application')->getMvcEvent()->getRequest();
        if (!$request->isPost()) {
            return;
        }

        $siteSettings = $services->get('Omeka\Settings\Site');
        if (!$siteSettings->get('encyclopedia_enabled')) {
            return;
        }

        // Update the file settings.json.
        $this->createSettingsJson();
    }

    protected function createSettingsJson(): void
    {
        /**
         * @var \Omeka\Settings\Settings $settings
         * @var \Omeka\Settings\SiteSettings $siteSettings
         * @var \Omeka\Api\Representation\SiteRepresentation $site
         * @var \Common\Stdlib\EasyMeta $easyMeta
         */
        $services = $this->getServiceLocator();
        $config = $services->get('Config');
        $plugins = $services->get('ControllerPluginManager');
        $helpers = $services->get('ViewHelperManager');
        $url = $helpers->get('url');
        $api = $services->get('Omeka\ApiManager');
        $settings = $services->get('Omeka\Settings');
        $assetUrl = $helpers->get('assetUrl');
        $siteSettings = $services->get('Omeka\Settings\Site');
        $translator = $services->get('MvcTranslator');
        $easyMeta = $services->get('EasyMeta');

        $data = [
            'sites' => [],
            'paths' => [],
            'header' => [],
            'notices' => [],
            'backgrounds' => [],
            'collections' => [],
            'browse' => [],
            'linkTypes' => [],
        ];

        // Site.
        $site = $plugins->get('currentSite')();
        $themePath = $site->theme();

        $getAsset = function (string $setting) use ($api, $siteSettings): ?AssetRepresentation {
            $assetId = (int) $siteSettings->get($setting);
            if (!$assetId) {
                return null;
            }
            try {
                return $api->read('assets', ['id' => $assetId])->getContent();
            } catch (NotFoundException $e) {
                return null;
            }
        };

        $fallbackLocale = $settings->get('locale')
            ?: ($translator->getDelegatedTranslator()->getLocale()
                ?: 'en');

        // The locale should be a 2 characters string to match values.
        $siteLocale = $siteSettings->get('locale') ?: $fallbackLocale;
        $siteLocaleShort = mb_substr($siteLocale, 0, 2);

        $data['sites'] = [
            $siteLocaleShort => [
                'id' => $site->id(),
                'slug' => $site->slug(),
                'lang' => $siteLocaleShort,
            ]
        ];

        $basePath = $helpers->get('BasePath')() ?: '/';
        $basePath = rtrim($basePath, '/') . '/';
        // $serverUrlBase = $helpers->get('ServerUrl')($helpers->get('BasePath')());

        $data['paths'] = [
            'public' => $basePath . 'site/',
            'api' => $basePath . 'api',
            'download' => $basePath . 's/collections',
            'admin' => $basePath . 'admin',
            'publisher' => $siteSettings->get('encyclopedia_publisher_url')
                ?: $url('top', [], ['force_canonical' => true]),
        ];

        $siteLogo = $getAsset('encyclopedia_site_logo') ?: $site->thumbnail();

        $data['header'] = [
            'logo' => $siteLogo ? $siteLogo->assetUrl() : null,
        ];

        $data['notices'] = [
            $siteLocaleShort => [
                'project' => $siteSettings->get('encyclopedia_site_name') ?: $site->title(),
                'footer' => $siteSettings->get('encyclopedia_site_description') ?: $site->summary(),
            ],
        ];

        if ($this->isModuleActive('Internationalisation')) {
            $siteGroups = $settings->get('internationalisation_site_groups', []);
            if (isset($siteGroups[$site->slug()]) && count($siteGroups[$site->slug()]) > 1) {
                foreach ($siteGroups[$site->slug()] as $nextSlug) {
                    try {
                        $siteNext = $api->read('sites', ['slug' => $nextSlug])->getContent();
                        $siteIdNext = $siteNext->id();
                    } catch (\Exception $e) {
                        continue;
                    }
                    $siteLocaleNext = (string) $siteSettings->get('locale', null, $siteIdNext);
                    $siteLocaleShortNext = mb_substr($siteLocaleNext, 0, 2);
                    if ($siteLocaleShortNext && !isset($data['sites'][$siteLocaleShortNext])) {
                        $data['sites'][$siteLocaleShortNext] = [
                            'id' => $siteIdNext,
                            'slug' => $nextSlug,
                            'lang' => $siteLocaleShortNext,
                        ];
                        $data['notices'][$siteLocaleShortNext] = [
                            'project' => $siteSettings->get('encyclopedia_site_name', null, $siteIdNext) ?: $siteNext->title(),
                            'footer' => $siteSettings->get('encyclopedia_site_description', null, $siteIdNext) ?: $siteNext->summary(),
                        ];
                    }
                }
            }
        }

        $homeBackground = $getAsset('encyclopedia_home_background');
        $publisherLogo = $getAsset('encyclopedia_publisher_logo');
        $publisherBackground = $getAsset('encyclopedia_publisher_background');

        $data['backgrounds'] = [
            'home-background' => $homeBackground
                ? $homeBackground->assetUrl()
                : "$basePath/themes/$themePath/asset/img/home-background.png",
            'publisher-logo' => $publisherLogo
                ? $publisherLogo->assetUrl()
                : "$basePath/themes/$themePath/asset/img/publisher-logo.png",
            'publisher-background' => $publisherBackground
                ? $publisherBackground->assetUrl()
                : "$basePath/themes/$themePath/asset/img/publisher-logo-background.png",
        ];

        $itemSetsIds = $siteSettings->get('encyclopedia_item_sets');
        $sourceIcon = $siteSettings->get('encyclopedia_item_set_icon') ?: 'theme';
        $sourceColor = $siteSettings->get('encyclopedia_item_set_color') ?: 'theme';
        $sourceIsEncyclopedia = $siteSettings->get('encyclopedia_item_set_encyclopedia') ?: 'theme';
        $sourceIsFacet = $siteSettings->get('encyclopedia_item_set_facet') ?: 'theme';
        $sourceRoute = $siteSettings->get('encyclopedia_item_set_route') ?: 'theme';
        $sourceLinkedLabel = $siteSettings->get('encyclopedia_item_set_linked_label') ?: 'theme';
        $sourceViews = $siteSettings->get('encyclopedia_item_set_views') ?: 'theme';
        $sourceViewDefault = $siteSettings->get('encyclopedia_item_set_view_default') ?: 'theme';
        $sourceItemType = $siteSettings->get('encyclopedia_item_set_item_type') ?: 'theme';

        $itemSets = $itemSetsIds ? $api->search('item_sets', ['id' => $itemSetsIds])->getContent() : [];
        if (!$itemSets) {
            foreach ($site->siteItemSets() as $siteItemSet) {
                $itemSets[] = $siteItemSet->itemSet();
            }
        }

        $defaultColors = [
            'person' => '#7BF478',
            'person_group' => '#19D8D3',
            'event' => '#FECA2A',
            'concept' => '#DACCB5',
            'practices' => '#E55050',
            'research' => '#A2ADF2',
            'works' => '#CF8DE5',
        ];
        $alpha = 96;
        $colorKey = 0;

        $data['collections'] = [];
        $matches = [];

        /** @var \Omeka\Api\Representation\ItemSetRepresentation $itemSet */
        foreach ($itemSets as $itemSet) {
            $collection = [
                'id' => $itemSet->id(),
                'title' => $itemSet->displayTitle(),
                'icon' => null,
                'color' => null,
                'encyclopedie' =>  true,
                'facet' => true,
                'route' => null,
                'linkedElementsLabel' => 'Associé à',
                'views' => 'tree-gallery-item',
                'defaultView' => 'gallery',
                'itemType' => 'item',
            ];

            // Icon.

            $icon = null;
            if ($sourceIcon === 'thumbnail') {
                $icon = $itemSet->thumbnail();
                if ($icon) {
                    $icon = $icon->assetUrl();
                }
            } elseif ($easyMeta->propertyId($sourceIcon)) {
                $iconValue = $itemSet->value($sourceIcon);
                if ($iconValue) {
                    $icon = ($vr = $iconValue->valueResource())
                        ? $vr->thumbnail()
                        : ($iconValue->uri() ?: $iconValue->value());
                    if (strpos($icon, '..') !== false) {
                        $icon = null;
                    } elseif (mb_substr($icon, 0, 8) !== 'https://' && mb_substr($icon, 0, 7) !== 'http://') {
                        // Nothing to do.
                    } elseif (mb_substr($icon, 0, mb_strlen("{$basePath}files/asset/")) === "{$basePath}files/asset/") {
                        // Nothing to do.
                    } elseif (mb_substr($icon, 0, mb_strlen("{$basePath}themes/$themePath/asset/img/")) === "{$basePath}themes/$themePath/asset/img/") {
                        // Nothing to do.
                    } elseif (mb_substr($icon, 0, 1) === '/'  || mb_strpos($icon, OMEKA_PATH) !== 0) {
                        // For security.
                        $icon = null;
                    } else {
                        if (mb_strpos($icon, OMEKA_PATH) === 0) {
                            $icon = mb_substr($icon, mb_strlen(OMEKA_PATH));
                        }
                        $icon = $assetUrl(trim($icon, '/'));
                    }
                }
            } elseif ($sourceIcon === 'theme') {
                $icon = 'img/icon/icon_' . $itemSet->value('dcterms:identifier');
                if (!file_exists(OMEKA_PATH . '/asset/' . $icon)) {
                    $icon = 'img/icon/icon_' . $itemSet->id();
                    if (!file_exists(OMEKA_PATH . '/asset/' . $icon)) {
                        $icon = 'img/icon/icon_' . chr(++$alpha % 10 + 97);
                    }
                }
                $icon = $assetUrl($icon);
            }
            $collection['icon'] = $icon;

            // Color.

            // Loop between colors when more than 7.
            $color = null;
            if ($easyMeta->propertyId($sourceColor)) {
                $colorValue = $itemSet->value($sourceColor);
                if ($colorValue) {
                    $colorValue = trim((string) $colorValue->value());
                    if (preg_match('~^#?(?:\d{3}|\d{6}|\d{8})$~', $colorValue, $matches)) {
                        $color = $colorValue;
                    }
                }
            }
            if (!$color) {
                $color = array_values($defaultColors)[++$colorKey % count($defaultColors)];
            }
            $collection['color'] = $color;

            // Is encyclopedia.

            $isEncyclopedia = false;
            if ($easyMeta->propertyId($sourceIsEncyclopedia)) {
                $isEncyclopediaValue = $itemSet->value($sourceIsEncyclopedia);
                if ($isEncyclopediaValue) {
                    $isEncyclopediaValue = $isEncyclopediaValue->value();
                    $isEncyclopedia = in_array($isEncyclopediaValue, [1, '1', 'true', true], true);
                }
            }
            $collection['encyclopedie'] = $isEncyclopedia;

            // Is facet.

            $isFacet = false;
            if ($easyMeta->propertyId($sourceIsFacet)) {
                $isFacetValue = $itemSet->value($sourceIsFacet);
                if ($isFacetValue) {
                    $isFacetValue = $isFacetValue->value();
                    $isFacet = in_array($isFacetValue, [1, '1', 'true', true], true);
                }
            }
            $collection['facet'] = $isFacet;

            // Route.

            $route = null;
            if ($easyMeta->propertyId($sourceRoute)) {
                $routeValue = $itemSet->value($sourceRoute);
                if ($routeValue) {
                    $routeValue = trim((string) $routeValue->value());
                    $route = $this->slugify($routeValue);
                }
            }
            if (!$route) {
                $routeValue = $itemSet->value('dcterms:identifier');
                if ($routeValue) {
                    $routeValue = trim((string) $routeValue->value());
                    $route = $this->slugify($routeValue);
                }
            }
            if (!$route) {
                $routeValue = $itemSet->value('dcterms:title');
                if ($routeValue) {
                    $routeValue = trim((string) $routeValue->value());
                    $route = $this->slugify($routeValue);
                }
            }
            $collection['route'] = $route;

            // Linked element label.

            $linkedLabel = null;
            if ($easyMeta->propertyId($sourceLinkedLabel)) {
                $linkedLabelValue = $itemSet->value($sourceLinkedLabel);
                if ($linkedLabelValue) {
                    $linkedLabel = trim((string) $linkedLabelValue->value());
                }
            }
            $collection['linkedElementsLabel'] = $linkedLabel;

            // Views.

            $views = null;
            if ($easyMeta->propertyId($sourceViews)) {
                $viewsValue = $itemSet->value($sourceViews);
                if ($viewsValue) {
                    $views = trim((string) $viewsValue->value());
                }
            }
            $collection['views'] = $views;

            // Default view.

            $defaultView = null;
            if ($easyMeta->propertyId($sourceViewDefault)) {
                $defaultViewValue = $itemSet->value($sourceViewDefault);
                if ($defaultViewValue) {
                    $defaultView = trim((string) $defaultViewValue->value());
                }
            }
            $collection['defaultView'] = $defaultView;

            // Item type.

            $itemType = null;
            if ($easyMeta->propertyId($sourceItemType)) {
                $itemTypeValue = $itemSet->value($sourceItemType);
                if ($itemTypeValue) {
                    $itemType = trim((string) $itemTypeValue->value());
                }
            }
            $collection['itemType'] = $itemType;

            $data['collections'][] = array_filter($collection, fn($v) => !is_null($v));
        }

        // Browse.

        $data['browse'] = [];
        $customVocabIds = $siteSettings->get('encyclopedia_browse_custom_vocabs') ?: [];
        foreach ($customVocabIds as $customVocabId) {
            $customVocabId = is_numeric($customVocabId) ? (int) $customVocabId : (int) substr($customVocabId, 12);
            try {
                /** @var \CustomVocab\Api\Representation\CustomVocabRepresentation $customVocab  */
                $customVocab = $api->read('custom_vocabs', $customVocabId)->getContent();
            } catch (\Exception $e) {
                continue;
            }
            $itemSet = $customVocab->itemSet();
            if (!$itemSet) {
                continue;
            }
            $data['browse'][] = [
                'id' => $itemSet->id(),
                'title' => $customVocab->label(),
                'customvocabs' => [$customVocab->id()]
            ];
        }

        // Link types.

        $defaultLinkTypes = [
            [
                'term' => 'skos:broader',
                'title' => 'Lien générique',
                'menuOrder' => 1
            ],
            [
                'term' => 'skos:narrower',
                'title' => 'Lien spécifique',
                'dashStyle' => '6 4',
                'menuOrder' => 2
            ],
            [
                'term' => 'skos:closeMatch',
                'title' => 'Lien proche',
                'dashStyle' => '2 3',
                'menuOrder' => 3
            ],
            [
                'term' => 'skos:broadMatch',
                'title' => 'Lien associé',
                'dashStyle' => '8 12',
                'strokeStyle' => '#FFFFFF',
                'menuOrder' => 4,
            ],
        ];

        $linkTypes = $siteSettings->get('encyclopedia_link_types', $defaultLinkTypes) ?: [];
        foreach (array_values($linkTypes) as $index => &$linkType) {
            $linkType['menuOrder'] = ++$index;
        }
        unset($linkType);
        $data['linkTypes'] = $linkTypes;

        // Save data.

        $basePath = $config['file_store']['local']['base_path'] ?: (OMEKA_PATH . '/files');
        $dirPath = $basePath . '/encyclopedia';
        $filePath = $dirPath . '/settings.json';
        $result = file_put_contents($filePath, json_encode($data, 448));
        if (!$result) {
            $message = new PsrMessage(
                $translator->translate('The file "{file}" cannot be stored. Check your rights.'), // @translate
                ['file' => 'files/encyclopedia/settings.json']
            );
            $messenger = $services->get('ControllerPluginManager')->get('messenger');
            $messenger->addError($message);
        }
    }

    /**
     * Transform the given string into a valid URL slug
     *
     * @param string $input
     * @return string
     */
    protected function slugify($input)
    {
        if (extension_loaded('intl')) {
            $transliterator = \Transliterator::createFromRules(':: NFD; :: [:Nonspacing Mark:] Remove; :: NFC;');
            $slug = $transliterator->transliterate($input);
        } elseif (extension_loaded('iconv')) {
            $slug = iconv('UTF-8', 'ASCII//TRANSLIT//IGNORE', $input);
        } else {
            $slug = $input;
        }
        $slug = mb_strtolower($slug, 'UTF-8');
        $slug = preg_replace('/[^a-z0-9-]+/u', '-', $slug);
        $slug = preg_replace('/-{2,}/', '-', $slug);
        $slug = preg_replace('/-*$/', '', $slug);
        return $slug;
    }
}
