��          L      |       �   �   �   *   *  	   U  �   _     �  #  �  q     '   �     �  �   �     C                                         A third-party front end is specified, but the directory of the theme selected for this site does not contain such a front theme. Enable third-party front end for the theme Front End If empty, the file "front/settings.json" of the theme will be used. This feature is only useful if the third-party front end uses it. Settings (json) Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-01-04 00:00+0000
Last-Translator: Daniel Berthereau <Daniel.fr@Berthereau.net>
Language-Team: 
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.4.2
 Un frontal tiers est activé, mais le dossier du thème sélectionné pour le site ne contient pas un tel thème. Activer le frontal tiers pour le thème Frontal Si vide, le fichier "front/settings.json" du thème sera utilisé. Cette fonctionnalité est utile seulement si le frontal l’utilise. Paramètres (json) 