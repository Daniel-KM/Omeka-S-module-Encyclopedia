Encyclopedia (module for Omeka S)
==============================

> __New versions of this module and support for Omeka S version 3.0 and above
> are available on [GitLab], which seems to respect users and privacy better
> than the previous repository.__

[Encyclopedia] is a module for [Omeka S] that manages the associate visual and
innovative [theme Encyclopedia] used in the sites [Hyper Otlet] and [Halbwachs].
The theme is not a standard digital library front-end, but designed for
semantic and pedagogical projects. It allows the user to discover the data
interactively and is first intended for encyclopedias.

The theme is built with the javascript platform [Vue.js].


Installation
------------

See general end user documentation for [installing a module].

The module [Common] must be installed first.

The module uses external libraries, so use the release zip to install it, or
use and init the source.

* From the zip

Download the last release [Encyclopedia.zip] from the list of releases (the
master does not contain the dependency), and uncompress it in the `modules`
directory.

* From the source and for development

If the module was installed from the source, rename the name of the folder of
the module to `Encyclopedia`.


Usage
-----

Fill the options of the module in the site settings form to specify how the
theme should display data. Then, the settings are stored in the file
files/encyclopedia/settings.json.

**WARNING** For now, the site settings should be "Submitted" each time an
element is updated, in particular item sets.


TODO
----

- [ ] Use the standard theme as a fallback when javascript is disabled.
- [ ] Make compatible with CleanUrl when the main slug is skipped.
- [ ] Use index.phtml if available.
- [ ] Remove enabling and check directly the theme?
- [ ] Make the settings fully dynamics on api or a specific controller.


Warning
-------

Use it at your own risk.

It’s always recommended to backup your files and your databases and to check
your archives regularly so you can roll back if needed.


Troubleshooting
---------------

See online issues on the [module issues] page.


License
-------

This plugin is published under the [CeCILL v2.1] license, compatible with
[GNU/GPL] and approved by [FSF] and [OSI].

In consideration of access to the source code and the rights to copy, modify and
redistribute granted by the license, users are provided only with a limited
warranty and the software’s author, the holder of the economic rights, and the
successive licensors only have limited liability.

In this respect, the risks associated with loading, using, modifying and/or
developing or reproducing the software by the user are brought to the user’s
attention, given its Free Software status, which may make it complicated to use,
with the result that its use is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore encouraged
to load and test the suitability of the software as regards their requirements
in conditions enabling the security of their systems and/or data to be ensured
and, more generally, to use and operate it in the same conditions of security.
This Agreement may be freely reproduced and published, provided it is not
altered, and that no provisions are either added or removed herefrom.


Copyright
---------

* Copyright Daniel Berthereau, 2020-2023 (see [Daniel-KM] on GitLab)

First version of this module was done for [Enssib] for the project [Hyper Otlet].
This generic version was designed for the project [Halbwachs] and for future
encyclopedia.


[Encyclopedia]: https://gitlab.com/Daniel-KM/Omeka-S-module-Encyclopedia
[GitLab]: https://gitlab.com/Daniel-KM/Omeka-S-module-Encyclopedia
[Omeka S]: https://www.omeka.org/s
[Vue.js]: https://vuejs.org/
[Hyper Otlet]: http://hyperotlet-collections.huma-num.fr
[Halbwachs]: http://Halbwachs.huma-num.fr
[installing a module]: https://omeka.org/s/docs/user-manual/modules/#installing-modules
[Common]: https://gitlab.com/Daniel-KM/Omeka-S-module-Common
[module issues]: https://gitlab.com/Daniel-KM/Omeka-S-module-Encyclopedia/-/issues
[CeCILL v2.1]: https://www.cecill.info/licences/Licence_CeCILL_V2.1-en.html
[GNU/GPL]: https://www.gnu.org/licenses/gpl-3.0.html
[FSF]: https://www.fsf.org
[OSI]: http://opensource.org
[Enssib]: https://www.enssib.fr
[Daniel-KM]: https://gitlab.com/Daniel-KM "Daniel Berthereau"
